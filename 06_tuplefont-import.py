# [h] import TupleFont from external file

#---------
# imports
#---------

import tuplefont
reload(tuplefont)

from tuplefont import TupleFont

#------
# data
#------

glyphs = {
    'A' : [
        (0.0, 0.0),
        (0.5, 1.0),
        (1.0, 0.0),
        (0.0, 0.0),
    ],
    'R' : [
        (0.0, 0.0),
        (0.0, 1.0),
        (1.0, 1.0),
        (1.0, 0.5),
        (0.5, 0.5),
        (1.0, 0.0),
    ],
    'S' : [
        (1.0, 0.8),
        (1.0, 1.0),
        (0.0, 1.0),
        (0.0, 0.6),
        (1.0, 0.4),
        (1.0, 0.0),
        (0.0, 0.0),
        (0.0, 0.2),
    ],
    'W' : [
        (0.0, 1.0),
        (0.0, 0.0),
        (0.5, 0.5),
        (1.0, 0.0),
        (1.0, 1.0),
    ], 
}

#------------
# parameters
#------------

x, y = 75, 380

parameters = {
    'text' : 'WARSAW',
    'text_spacing' : .8,
    'width' : 64,
    'height' : 200,
    # guides
    'guides_draw' : False,
    'guides_width' : 2,
    'guides_color' : color(.9),
    # points
    'points_draw' : True,
    'points_size' : 5,
    'points_color' : color(0),
    # stroke
    'stroke_draw' : True,
    'stroke_color' : color(0),
    'stroke_width' : 1,
    # pen
    'pen_draw' : True,  
    'pen_width' : 30,
    'pen_height' : 10,
    'pen_angle' : 30,
    'pen_distance' : 7,
    'pen_color_mode' : 1,
    'pen_alpha' : .5,
    'pen_fill_color' : color(0, 1, 0),
    'pen_stroke_color' : color(0, 1, 1),
    'pen_stroke_width' : 0,
}

#-------
# draw!
#-------

size(800, 600)
background(1)

f = TupleFont(_ctx, glyphs)
f.set_parameters(parameters)
f.draw((x, y))

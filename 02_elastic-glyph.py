# [h] drawing an elastic glyph within a box

#-----------
# functions
#-----------

def draw_box((x, y), (w, h), c):
    # set stroke
    strokewidth(1)
    stroke(c)
    # draw lines
    line(0, y, WIDTH, y)
    y -= h
    line(0, y, WIDTH, y)
    line(x, 0, x, HEIGHT)
    x += w
    line(x, 0, x, HEIGHT)

def draw_line(points_list, stroke_color, stroke_width):
    # set line properties
    nofill()
    autoclosepath(close=False)
    stroke(stroke_color)
    strokewidth(stroke_width)
    # draw line
    beginpath()
    for i, p in enumerate(points_list):
        x, y = p
        if i == 0:
            moveto(x, y)
        else:
            lineto(x, y)
    endpath()

def draw_points(points_list, point_size, points_color):
    # set point properties
    nostroke()
    fill(points_color)
    # draw points
    for p in points_list:
        x, y = p
        x -= (point_size/2.0)
        y -= (point_size/2.0)
        oval(x, y, point_size, point_size)

def make_points(points_list, (x, y), (w, h)):
    # calculate points for current box
    glyph_points = []
    for p in points_list:
        sx, sy = p
        _x = x + (w * sx)
        _y = y - (h * sy)
        glyph_points.append((_x, _y))
    # return points list
    return glyph_points

#------
# data
#------

# glyph descriptions

glyphs = {
    'A' : [
        (0, 0),
        (0.5, 1),
        (1, 0),
    ],
    'R' : [
        (0, 0),
        (0, 1),
        (1, 1),
        (1, .5),
        (.5, .5),
        (1, 0),
    ],
    'S' : [
        (1, 1),
        (0, 1),
        (0, .6),
        (1, .4),
        (1, 0),
        (0, 0),
    ],
    'W' : [
        (0, 1),
        (0, 0),
        (.5, .5),
        (1, 0),
        (1, 1),
    ], 
}

#------------
# parameters
#------------

x, y = 122, 375
w, h = 297, 245

guides_color = color(.5)

points_size = 38
points_color = color(1, 0, 0, .75)

stroke_color = color(0, 1, 0)
stroke_width = 9

#-------
# draw!
#-------

# set canvas size
size(600, 500)

# draw elastic box
draw_box((x, y), (w, h), guides_color)

# calculate points for box
points_list = make_points(glyphs['R'], (x, y), (w, h))

# draw line between points
draw_line(points_list, stroke_color, stroke_width)

# draw points
draw_points(points_list, points_size, points_color)


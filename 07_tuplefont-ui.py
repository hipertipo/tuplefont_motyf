# [h] TupleFont with dialog

#---------
# imports
#---------

from tuplefont import TupleFont

# glyphs are imported from external file:

import myfont1
reload(myfont1)

from myfont1 import glyphs

#-----------
# variables
#-----------

size(960, 600)
background(1)

var('_text', TEXT, default='WARSAW')
var('w', NUMBER, 66, 10, 200)
var('h', NUMBER, 88, 10, 200)
var('x', NUMBER, 90, 0, WIDTH)
var('y', NUMBER, 360, 0, HEIGHT)
# points
var('_points_draw', BOOLEAN)
var('_points_size', NUMBER, 2, 1, 100)
# stroke
var('_stroke_draw', BOOLEAN)
var('_stroke_width', NUMBER, 2, 1, 20)
# pen parameters
var('_pen_draw', BOOLEAN)
var('_pen_width', NUMBER, 20, 1, 200)
var('_pen_heigth', NUMBER, 20, 1, 200)
var('_pen_angle', NUMBER, 35, 0, 180)
var('_pen_distance', NUMBER, 10, 1, 30)
var('_pen_alpha', NUMBER, .5, 0, 1.0)
var('_pen_color', BOOLEAN)

#------------
# parameters
#------------

parameters = {
    'text' : _text,
    'text_spacing' : .7,
    'width' : w,
    'height' : h,
    # guides
    'guides_draw' : False,
    'guides_width' : 2,
    'guides_color' : color(.9),
    # points
    'points_draw' : _points_draw,
    'points_size' : _points_size,
    'points_color' : color(0),
    # stroke
    'stroke_draw' : _stroke_draw,
    'stroke_color' : color(0),
    'stroke_width' : _stroke_width,
    # pen
    'pen_draw' : _pen_draw,  
    'pen_width' : _pen_width,
    'pen_height' : _pen_heigth,
    'pen_angle' : _pen_angle,
    'pen_distance' : _pen_distance,
    'pen_color_mode' : _pen_color,
    'pen_alpha' : _pen_alpha,
    'pen_fill_color' : color(0, 1, 0),
    'pen_stroke_color' : color(0, 1, 1),
    'pen_stroke_width' : 0,
}

#-------
# draw!
#-------

f = TupleFont(_ctx, glyphs)
f.set_parameters(parameters)
f.draw((x, y))

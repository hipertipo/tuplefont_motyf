# [h] TupleFont as an object

# before running this script, please
# download & install the Colors library:
# http://nodebox.net/code/index.php/Colors

#---------
# imports
#---------

colors = ximport('colors')

#---------
# objects
#---------

class TupleFont(object):
    
    width = 10
    height = 10    

    # text
    text = 'WARSAW'
    text_spacing = .5

    # guide
    guides_draw = True
    guides_color = 1
    guides_width = 1

    # point
    points_draw = True
    points_size = 21
    points_color = (1, 0, 0, .8)

    # stroke
    stroke_draw = True
    stroke_color = (0, 1, 0)
    stroke_width = 25 *.005

    #-----------
    # functions
    #-----------

    def __init__(self, ctx, glyphs):
        self.ctx = ctx
        self.glyphs = glyphs
        # set default colors
        self.guides_color = self.ctx.color(self.guides_color)
        self.points_color = self.ctx.color(self.points_color)
        self.stroke_color = self.ctx.color(self.stroke_color)

    def set_parameters(self, parameters):
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])

    def _make_points(self, points_list, (x, y)):
        glyph_points = []
        for p in points_list:
            sx, sy = p
            _x = x + (self.width * sx)
            _y = y - (self.height * sy)
            glyph_points.append((_x, _y))
        return glyph_points

    def _draw_box(self, (x, y)):
        # set box stroke
        self.ctx.nofill()
        self.ctx.strokewidth(self.guides_width)
        self.ctx.stroke(self.guides_color)
        # draw lines
        self.ctx.rect(x, y, self.width, -self.height)

    def _draw_line(self, points_list):
        # set line properties
        self.ctx.nofill()
        self.ctx.autoclosepath(close=False)
        self.ctx.stroke(self.stroke_color)
        self.ctx.strokewidth(self.stroke_width)
        # draw line
        self.ctx.beginpath()
        for i, p in enumerate(points_list):
            x, y = p
            if i == 0:
                self.ctx.moveto(x, y)
            else:
                self.ctx.lineto(x, y)
        path = self.ctx.endpath(self.stroke_draw)
        # set pen
        return path

    def _draw_points(self, points_list):
        # set points properties
        self.ctx.nostroke()
        self.ctx.fill(self.points_color)
        # draw points
        for p in points_list:
            x, y = p
            x -= (self.points_size/2.0)
            y -= (self.points_size/2.0)
            self.ctx.oval(x, y, self.points_size, self.points_size)

    def _draw_glyph(self, glyph_name, (x, y)):
        # make points
        points_list = self._make_points(self.glyphs[glyph_name], (x, y))
        # draw guides
        if self.guides_draw:
            self._draw_box((x, y))
        # draw strokes
        if self.stroke_draw:
            path = self._draw_line(points_list)
        # draw points
        if self.points_draw:
            self._draw_points(points_list)

    def draw(self, (x, y)):
        # draw glyphs
        for char in self.text:
            self._draw_glyph(char, (x, y))
            x += self.width + (self.width * self.text_spacing)

#------
# data
#------

glyphs = {
    'A' : [
        (0.0, 0.0),
        (0.5, 1.0),
        (1.0, 0.0),
        (0.0, 0.0),
    ],
    'R' : [
        (0.0, 0.0),
        (0.0, 1.0),
        (1.0, 1.0),
        (1.0, 0.5),
        (0.5, 0.5),
        (1.0, 0.0),
    ],
    'S' : [
        (1.0, 0.8),
        (1.0, 1.0),
        (0.0, 1.0),
        (0.0, 0.6),
        (1.0, 0.4),
        (1.0, 0.0),
        (0.0, 0.0),
        (0.0, 0.2),
    ],
    'W' : [
        (0.0, 1.0),
        (0.0, 0.0),
        (0.5, 0.5),
        (1.0, 0.0),
        (1.0, 1.0),
    ], 
}
    
#------------
# parameters
#------------

x, y = 70, 360

parameters = {
    'text' : 'WARSAW',
    'text_spacing' : 0.5,
    'width' : 91,
    'height' : 190,
    # guides
    'guides_draw' : True,
    'guides_width' : 2,
    'guides_color' : color(.9),
    # points
    'points_draw' : True,
    'points_size' : 16,
    'points_color' : color(1, 0, 0, .8),
    # stroke
    'stroke_draw' : True,
    'stroke_color' : color(.7),
    'stroke_width' : 6,
}

#-------
# draw!
#-------

size(960, 600)
background(1)

f = TupleFont(_ctx, glyphs)
f.set_parameters(parameters)
f.draw((x, y))

# [h] TupleFont object

#---------
# objects
#---------

class TupleFont(object):
    
    width = 10
    height = 10    

    # text
    text = 'WARSAW'
    text_spacing = .5

    # guide
    guides_draw = True
    guides_color = 1
    guides_width = 1

    # point
    points_draw = True
    points_size = 21
    points_color = (1, 0, 0, .8)

    # stroke
    stroke_draw = True
    stroke_color = (0, 1, 0)
    stroke_width = 25 *.005

    # pen
    pen_draw = False
    pen_distance = 10
    pen_width = 10
    pen_height = 10
    pen_angle = 10
    pen_color_mode = 1
    pen_alpha = .5
    pen_fill_color = .3
    pen_stroke_color = .5
    pen_stroke_width = 0

    #-----------
    # functions
    #-----------

    def __init__(self, ctx, glyphs):
        self.ctx = ctx
        self.ctx.colors = self.ctx.ximport('colors')
        self.glyphs = glyphs
        # set default colors
        self.guides_color = self.ctx.color(self.guides_color)
        self.points_color = self.ctx.color(self.points_color)
        self.stroke_color = self.ctx.color(self.stroke_color)

    def set_parameters(self, parameters):
        for parameter in parameters.keys():
            setattr(self, parameter, parameters[parameter])

    def _make_points(self, points_list, (x, y)):
        glyph_points = []
        for p in points_list:
            sx, sy = p
            _x = x + (self.width * sx)
            _y = y - (self.height * sy)
            glyph_points.append((_x, _y))
        return glyph_points

    def _draw_box(self, (x, y)):
        # set box stroke
        self.ctx.nofill()
        self.ctx.strokewidth(self.guides_width)
        self.ctx.stroke(self.guides_color)
        # draw lines
        self.ctx.rect(x, y, self.width, -self.height)

    def _draw_line(self, points_list):
        # set line properties
        self.ctx.nofill()
        self.ctx.autoclosepath(close=False)
        self.ctx.stroke(self.stroke_color)
        self.ctx.strokewidth(self.stroke_width)
        # draw line
        self.ctx.beginpath()
        for i, p in enumerate(points_list):
            x, y = p
            if i == 0:
                self.ctx.moveto(x, y)
            else:
                self.ctx.lineto(x, y)
        path = self.ctx.endpath(self.stroke_draw)
        # set pen
        return path

    def _draw_points(self, points_list):
        # set points properties
        self.ctx.nostroke()
        self.ctx.fill(self.points_color)
        # draw points
        for p in points_list:
            x, y = p
            x -= (self.points_size/2.0)
            y -= (self.points_size/2.0)
            self.ctx.oval(x, y, self.points_size, self.points_size)

    def _set_element_color(self, color_step, i):
        # make solid color
        if self.pen_color_mode == 0:
            _color = self.pen_fill_color
        # make variable color
        else:
            _hue = color_step * i
            _color = self.ctx.colors.hsb(_hue, 1, 1)
        # set color
        _color.alpha = self.pen_alpha
        self.ctx.fill(_color)

    def _draw_element(self, (x, y)):
        # draw pen
        self.ctx.push()
        # apply pen angle
        self.ctx.transform(mode=2)
        self.ctx.rotate(self.pen_angle)
        # set stroke
        self.ctx.stroke(self.pen_stroke_color)
        self.ctx.strokewidth(self.pen_stroke_width)
        # draw pen
        self.ctx.oval(x, y, self.pen_width, self.pen_height)
        # done
        self.ctx.pop()        

    def _draw_pen(self, path):
        # calculate steps
        pen_steps = int(path.length  / self.pen_distance)
        color_step = 1.0 / (pen_steps)
        # draw ellipses
        for i, pt in enumerate(path.points(pen_steps)):
            # make position
            x = pt.x - (self.pen_width/2.0)
            y = pt.y - (self.pen_height/2.0)
            # draw pen
            self._set_element_color(color_step, i)
            self._draw_element((x, y))

    def _draw_glyph(self, glyph_name, (x, y)):
        # make points
        points_list = self._make_points(self.glyphs[glyph_name], (x, y))
        # draw guides
        if self.guides_draw:
            self._draw_box((x, y))
        # draw strokes
        if self.stroke_draw:
            path = self._draw_line(points_list)
        # draw points
        if self.points_draw:
            self._draw_points(points_list)
        # draw pen
        if self.pen_draw:
            if not self.stroke_draw:
                path = self._draw_line(points_list)
            self._draw_pen(path)

    def draw(self, (x, y)):
        # draw glyphs
        for char in self.text:
            self._draw_glyph(char, (x, y))
            x += self.width + (self.width * self.text_spacing)

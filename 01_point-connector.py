# [h] point connector

'''drawing a line between a list of points'''

#-----------
# functions
#-----------

def draw_line(points_list):
    # set line properties
    nofill()
    autoclosepath(close=False)
    stroke(1, 0, 0)
    strokewidth(5)
    # draw line from points
    beginpath()
    for i, p in enumerate(points_list):
        x, y = p
        if i == 0:
            moveto(x, y)
        else:
            lineto(x, y)
    endpath()

def draw_points(points_list, point_size):
    # set point properties
    nostroke()
    fill(1, 0, 1, .5)
    # draw points
    for p in points_list:
        x, y = p
        x -= (point_size/2.0)
        y -= (point_size/2.0)
        oval(x, y, point_size, point_size)

#------
# data
#------

points = [
    (314, 83),
    (94, 120),
    (194, 376),
    (426, 236),
]

#-------
# draw!
#-------

size(600, 500)
draw_line(points)
draw_points(points, 30)

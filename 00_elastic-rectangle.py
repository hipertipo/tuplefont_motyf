# [h] draw elastic box

#-----------
# functions
#-----------

def draw_box((x, y), (w, h), c):
    # set stroke properties
    strokewidth(1)
    stroke(c)
    # draw horizontal lines
    line(0, y, WIDTH, y)
    y += h
    line(0, y, WIDTH, y)
    # draw vertical lines 
    line(x, 0, x, HEIGHT)
    x += w
    line(x, 0, x, HEIGHT)

#------------
# parameters
#------------

# x/y position
x, y = 98, 114

# width/height
w, h = 300, 200     

# stroke color
c = color(.5)

#-------
# draw!
#-------

size(600, 500)
draw_box((x, y), (w, h), c)

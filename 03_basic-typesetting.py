# [h] compose glyphs into a word

#-----------
# functions
#-----------

def draw_box((x, y), (w, h), c):
    # set stroke
    strokewidth(1)
    stroke(c)
    # draw lines
    line(0, y, WIDTH, y)
    y -= h
    line(0, y, WIDTH, y)
    line(x, 0, x, HEIGHT)
    x += w
    line(x, 0, x, HEIGHT)

def draw_line(points_list, stroke_color, stroke_width):
    # set line properties
    nofill()
    autoclosepath(close=False)
    stroke(stroke_color)
    strokewidth(stroke_width)
    # draw line
    beginpath()
    for i, p in enumerate(points_list):
        x, y = p
        if i == 0:
            moveto(x, y)
        else:
            lineto(x, y)
    endpath()

def draw_points(points_list, point_size, points_color):
    # set point properties
    nostroke()
    fill(points_color)
    # draw points
    for p in points_list:
        x, y = p
        x -= (point_size/2.0)
        y -= (point_size/2.0)
        oval(x, y, point_size, point_size)

def make_points(points_list, (x, y), (w, h)):
    # calculate points for current box
    glyph_points = []
    for p in points_list:
        sx, sy = p
        _x = x + (w * sx)
        _y = y - (h * sy)
        glyph_points.append((_x, _y))
    return glyph_points

def draw_glyph(glyph_name, (x, y)):
    # draw elastic box
    draw_box((x, y), (w, h), guides_color)
    # calculate points for box
    points_list = make_points(glyphs[glyph_name], (x, y), (w, h))
    # draw line between points
    draw_line(points_list, stroke_color, stroke_width)
    # draw points
    draw_points(points_list, points_size, points_color)

#------
# data
#------

# glyph descriptions

glyphs = {
    'A' : [
        (0.0, 0.0),
        (0.5, 1.0),
        (1.0, 0.0),
    ],
    'R' : [
        (0.0, 0.0),
        (0.0, 1.0),
        (1.0, 1.0),
        (1.0, 0.5),
        (0.5, 0.5),
        (1.0, 0.0),
    ],
    'S' : [
        (1.0, 1.0),
        (0.0, 1.0),
        (0.0, 0.6),
        (1.0, 0.4),
        (1.0, 0.0),
        (0.0, 0.0),
    ],
    'W' : [
        (0.0, 1.0),
        (0.0, 0.0),
        (0.5, 0.5),
        (1.0, 0.0),
        (1.0, 1.0),
    ], 
}

#------------
# parameters
#------------

_text = 'WARSAW'
_text_spacing = 50 * .01

x, y = 64, 366
w, h = 71, 112

guides_color = color(.8)

points_size = 20
points_color = color(1, 0, 0, .75)

stroke_color = color(0, 1, 0)
stroke_width = 11

#-------
# draw!
#-------

# set canvas size
size(800, 600)

# set text
for char in _text:
    draw_glyph(char, (x, y))
    x += w + (w * _text_spacing)
